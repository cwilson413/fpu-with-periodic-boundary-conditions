#1d Bravais Lattice with Born-Von Karman Boundry Conditions
#Non-linear Model, simulated using Velocity Verlet Algorithm

import pygame, math, sys
import matplotlib.pyplot as plt

#Simulation Global Variables
K = 1.0 				#Spring Constant
alpha = 0.25 			#Non-linearity
beta = 0.0
M = 1.0 				#Mass of all particles
N = 32 					#Number of particles
a = 2.0 				#Spacing
t = 0 					#time
mode = int(sys.argv[1])	#starting mode number
dt = 0.01				#change in time for simulation
amp = 1.0 				#amplitude of initial position displacement
oscillations = 500		#Number of oscillations to simulate
particle = []			#list of particles
k = []					#list of k values
omega = [] 				#list of omega values

#Display Global Variables
graph_type = False					#True = position vs time, False = energy vs time
GUI = False							#Whether or not the GUI should run
background_color = (255,255,255) 	#Screen background color (white)
width = 800 						#Width of display in pixels
height = 600						#Height of display in pixels
scale = 20 							#the spacing for display purposes
shift = 50							#shifts the particles right

#Particle Class
class Particle:

	def __init__(self, n, u, v):
		self.n = n
		self.x = n*a + u
		self.color = (0,0,255)
		self.v = v
		
	def display(self):
		#if self.n == 3:
		#	print(self.n, self.x)
		pygame.draw.circle(screen, self.color, (int(round( self.x * scale)) + shift, 300), 8, 1)

	def updatePosition(self):
		#a(t)
		acceleration = self.calcAcceleration()

		#x(t + dt)
		new_x = self.x + self.v * dt + acceleration * dt**2
		
		#v(t + dt)
		self.v = self.v + acceleration * dt

		return new_x

	def calcAcceleration(self):
		n0 = self.n
		n1 = (self.n + 1) % N
		n2 = (self.n - 1) % N

		u0 = self.x - n0 * a 			#u of this particle
		u1 = particle[n1].x - n1 * a 	#u of n+1
		u2 = particle[n2].x - n2 * a 	#u of n-1

		val = (K * (u1 + u2 - 2 * u0) + alpha * ((u2 - u0)**2  - (u1 - u0)**2) + beta * ((u1 - u0)**3  - (u0 - u2)**3)) / M

		return val

def updateDisplay(t, e, error, h):
	for p in particle:
		p.display()

	text = font.render("t = " + str(t), True, (0,0,255))
	text_rect = text.get_rect()
	text_x = 100
	text_y = 100
	screen.blit(text, [text_x, text_y])

	text = font.render("E = " + str(e), True, (0,0,255))
	text_rect = text.get_rect()
	text_x = 100
	text_y = 150
	screen.blit(text, [text_x, text_y])

	text = font.render("% error = " + str(error), True, (0,0,255))
	text_rect = text.get_rect()
	text_x = 100
	text_y = 200
	screen.blit(text, [text_x, text_y])

	text = font.render("H = " + str(h), True, (0,0,255))
	text_rect = text.get_rect()
	text_x = 100
	text_y = 250
	screen.blit(text, [text_x, text_y])

	pygame.display.flip()


def updateParticles():
	pos = []

	for i in range(N):
		pos.append(particle[i].updatePosition())

	for i in range(N):
		particle[i].x = pos[i]

def getEnergy():
	#Find the KE of each particle
	KE = 0
	for i in range(N):
		KE = KE + .5 * M * particle[i].v**2

	#Find the PE in each spring
	PE = 0
	for i in range(N):
		u0 = particle[i].x - particle[i].n * a
		if particle[i].n == N - 1:
			u1 = particle[0].x
		else:
			u1 = particle[particle[i].n + 1].x - (particle[i].n+1) * a
		
		PE = PE + .5 * K * (u0 - u1)**2 + alpha / 3 * (u0 - u1)**3 + beta / 4 * (u0-u1)**4

	return KE + PE

def getError(e, initE):
	return 100 * (e - initE) / initE

count = 0
def getH():
	h = 0
	for i in range(len(omega)):
		p = 0
		q = 0
		#s1 and s2 for q, s3 and s4 for p
		s1 = 0 
		s2 = 0
		s3 = 0
		s4 = 0
		for unit in particle:
			u = unit.x - unit.n * a

			s1 += u * math.cos(k[i] * unit.n * a) / N**.5
			s2 += u * math.sin(k[i] * unit.n * a) / N**.5
			
			s3 += M * unit.v * math.cos(k[i] * unit.n * a) / N**.5
			s4 += M * unit.v * math.sin(k[i] * unit.n * a) / N**.5

		q = s1**2 + s2**2
		p = s3**2 + s4**2
		
		hk = p / (2 * M) + M * omega[i]**2 * q / 2
		h += hk

	return h

def getModeEnergy():
	energy = []
	for i in range(len(omega)):
		p = 0
		q = 0
		#s1 and s2 for q, s3 and s4 for p
		s1 = 0 
		s2 = 0
		s3 = 0
		s4 = 0
		for unit in particle:
			u = unit.x - unit.n * a

			s1 += u * math.cos(k[i] * unit.n * a) / N**.5
			s2 += u * math.sin(k[i] * unit.n * a) / N**.5
			
			s3 += M * unit.v * math.cos(k[i] * unit.n * a) / N**.5
			s4 += M * unit.v * math.sin(k[i] * unit.n * a) / N**.5

		q = s1**2 + s2**2
		p = s3**2 + s4**2
		
		hk = p / (2 * M) + M * omega[i]**2 * q / 2
		energy.append(hk)
	return energy


def populateLists():
	#Populate k[]
	if N % 2 ==0:
		for i in range(N):
	 	   k.append(-math.pi/a+ i* 2*math.pi/N/a)			
	else:
		for i in range(N):
			k.append(-math.pi/a + 2*math.pi/N/a/2 + i*2*math.pi/N/a)

	#Populate omega[]
	for i in k:
		omega.append(math.sqrt(2 * (K/M) * (1.0 - math.cos(a*i))))

	#Populate particle[]values
	for i in range(N):
		particle.append(Particle(i, amp*math.cos(k[mode] * i * a), -omega[mode]*amp*math.sin(k[mode]* i * a)))


def graph():
	f = open('results.txt', 'r')
	x = []
	y = []
	z = []

	for i in range(N+1):
		y.append([])
		z.append([])

	for line in f:
		arr = line.split(" ")
		t = float(arr.pop(0))
		x.append(t)
		for i in range(len(arr)):
			y[i].append(arr[i])
			val = math.cos(k[mode]*i*a - omega[mode]*t)
			z[i].append(val)
	
	#yes we could loop here but this makes it easy to turn off plots by commenting them out
	if graph_type == True:

		plt.plot(x, y[0], label="0 Non-Linear")
		plt.plot(x, z[0], label="0 Linear")

		plt.plot(x, y[1], label="1 Non-Linear")
		plt.plot(x, z[1], label="1 Linear")

		plt.plot(x, y[2], label="2 Non-Linear")
		plt.plot(x, z[2], label="2 Linear")
		
		plt.plot(x, y[3], label="3 Non-Linear")
		plt.plot(x, z[3], label="3 Linear")
		
		plt.plot(x, y[4], label="4 Non-Linear")
		plt.plot(x, z[4], label="4 Linear")

		plt.plot(x, y[5], label="5 Non-Linear")
		plt.plot(x, z[5], label="5 Linear")
		
		plt.plot(x, y[6], label="6 Non-Linear")
		plt.plot(x, z[6], label="6 Linear")
		
		plt.plot(x, y[7], label="7 Non-Linear")
		plt.plot(x, z[7], label="7 Linear")


		plt.title('Displacement vs Time (Mode ' + str(mode) + ' initially excited)')
		plt.ylabel('Displacement')
		plt.ylim(-2,2)
	else:
		plt.plot(x, y[0], label=0)
		plt.plot(x, y[1], label=1)
		plt.plot(x, y[2], label=2)
		plt.plot(x, y[3], label=3)
		plt.plot(x, y[4], label=4)
		plt.plot(x, y[5], label=5)
		plt.plot(x, y[6], label=6)
		plt.plot(x, y[7], label=7)
		plt.plot(x, y[8], label=8)
		plt.plot(x, y[9], label=9)
		plt.plot(x, y[10], label=10)
		plt.plot(x, y[11], label=11)
		plt.plot(x, y[12], label=12)
		plt.plot(x, y[13], label=13)
		plt.plot(x, y[14], label=14)
		plt.plot(x, y[15], label=15)

		plt.plot(x, y[N], label='Total')
		plt.title('Energy vs Time (Mode ' + str(mode) + ' initially excited)')
		plt.ylabel('Energy')

	plt.xlabel('Time')
	plt.legend(loc=1)	
	plt.xlim(0, finish_time)

	plt.show()

if GUI:
	pygame.init()
	pygame.display.set_caption('System 1')				#sets window title
	screen = pygame.display.set_mode((width, height)) 	#creates a window object, called screen
	font = pygame.font.Font(None, 36)					#font to draw text on screen

populateLists()

initE = getEnergy()
counter = 0
print("N: " + str(N))
print("K: " + str(K))
print("Alpha: " + str(alpha))
print("M: " + str(M))
print("a: " + str(a))
print("dt: " + str(dt))
print("Mode: "+ str(mode))
print("Frequency: " + str(omega[mode]) + " rad/s")
print("Period: " + str(2*math.pi / omega[mode]))
finish_time = oscillations*2*math.pi/omega[mode]
print("Time to " + str(oscillations) + " oscillations: " + str(finish_time))
print("Initial Energy: " + str(initE))

running = True
if GUI:
	while running:
	   
	    for event in pygame.event.get():
	        if event.type == pygame.QUIT:
	            running = False
	    screen.fill(background_color)
	    updateParticles()
	    t = t + dt
	    e = getEnergy()
	    h = getH()
	    error = getError(e, initE)
	    updateDisplay(t, e, error, h)
	    if t > finish_time:
	    	print("Final % error: " + str(error))
	    	print(str(oscillations) + " oscillations achieved")
	    	running = False
elif graph_type:
	f = open('results.txt', 'w')
	while running:
		updateParticles()
		t = t + dt
		msg = str(t) + " "
		for i in particle:
			msg = msg + str(i.x - i.n * a) + " "
		f.write(msg + "\n")
		
		if t > finish_time:
			print("Final % error: " + str(getError(getEnergy(), initE)))
			print(str(oscillations) + " oscillations achieved")
			f.close()
			graph()
			running = False	
else:
	f = open('results.txt', 'w')
	while running:
		updateParticles()
		t = t + dt
		msg = str(t)
		for i in getModeEnergy():
			msg = msg + " " + str(i)
		f.write(msg + " " + str(getEnergy()) + "\n")

		if t > finish_time:
			print("Final % error: " + str(getError(getEnergy(), initE)))
			print(str(oscillations) + " oscillations achieved")
			f.close()
			graph()
			running = False	