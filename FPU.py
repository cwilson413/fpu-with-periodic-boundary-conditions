import pygame, math, sys
import matplotlib.pyplot as plt

#Simulation Global Variables
K = 1.0 				#Spring Constant
alpha = 0.25 			#Non-linearity
beta = 0.0
M = 1.0 				#Mass of all particles
N = 32 					#Number of particles
t = 0 					#time
mode = int(sys.argv[1])	#starting mode number
dt = 0.001				#change in time for simulation
oscillations = 50.0		#Number of oscillations to simulate
particle = []			#list of particles
omega = [] 				#list of omega values

#Display Global Variables
graph_type = False					#True = position vs time, False = energy vs time
GUI = False							#Whether or not the GUI should run
background_color = (255,255,255) 	#Screen background color (white)
width = 800 						#Width of display in pixels
height = 600						#Height of display in pixels
scale = 50 							#the spacing for display purposes
shift = 50							#shifts the particles right

#Particle Class
class Particle:

	def __init__(self, n, u, v):
		self.n = n
		self.x = n + u
		self.color = (0,0,255)
		self.v = v
		
	def display(self):
		pygame.draw.circle(screen, self.color, (int(round( self.x * scale)) + shift, 300), 8, 1)

	def getU(self):
		return self.x - self.n

	def updatePosition(self):
		#a(t)
		self.acceleration = self.calcAcceleration()

		#x(t + dt)
		new_x = self.x + self.v * dt + self.acceleration * dt**2

		return new_x

	def updateV(self):
		#Standard velocity verlet velocity update
		#self.v = self.v + dt * (self.acceleration + self.calcAcceleration()) / 2.0

		#More simplistic velocity update
		self.v = self.v + dt * self.acceleration

	def calcAcceleration(self):
		u0 = self.getU() 					#u of this particle
		u1 = particle[self.n + 1].getU() 	#u of n+1
		u2 = particle[self.n - 1].getU()		#u of n-1

		val = (K*(u1 + u2 - 2*u0) + alpha * ((u2 - u0)**2  - (u1 - u0)**2) + beta * ((u1 - u0)**3  - (u0 - u2)**3)) / M

		return val

def updateParticles():
	pos = []

	for i in range(1,N+1):
		pos.append(particle[i].updatePosition())

	for i in range(1,N+1):
		particle[i].x = pos[i-1]

	for i in range(1,N+1):
		particle[i].updateV()

def getEnergy():
	h = getModeEnergy()
	hsum = 0
	for i in h:
		hsum += i
	return hsum
	'''

	KE = 0
	PE = 0
	for i in range(0,N+1):
		#KE
		KE += .5 * M * particle[i].v**2

		#PE
		u0 = particle[i].getU()
		u1 = particle[i+1].getU()
		PE += .5 * K * (u0 - u1)**2 + alpha / 3.0 * (u0 - u1)**3 + beta / 4.0 * (u0-u1)**4

	return KE + PE
	'''

def getModeEnergy():
	energy = []
	for i in range(1,N+1):
		A = 0
		Adot = 0
		for j in range(1,N+1):
			A += (particle[j].getU())*math.sin(j*i*math.pi/(N+1))
			Adot += particle[j].v*math.sin(j*i*math.pi/(N+1))

		energy.append(.5*Adot**2 + 2 * math.sin(i * math.pi / (2.0 * N + 2))**2 * A**2)

	return energy


def getError(n, m):
	return 0

def updateDisplay(t, e, error, h):
	for p in particle:
		p.display()

	text = font.render("t = " + str(t), True, (0,0,255))
	text_rect = text.get_rect()
	text_x = 100
	text_y = 100
	screen.blit(text, [text_x, text_y])

	text = font.render("E = " + str(e), True, (0,0,255))
	text_rect = text.get_rect()
	text_x = 100
	text_y = 150
	screen.blit(text, [text_x, text_y])

	text = font.render("% error = " + str(error), True, (0,0,255))
	text_rect = text.get_rect()
	text_x = 100
	text_y = 200
	screen.blit(text, [text_x, text_y])

	text = font.render("H = " + str(h), True, (0,0,255))
	text_rect = text.get_rect()
	text_x = 100
	text_y = 250
	screen.blit(text, [text_x, text_y])

	pygame.display.flip()


def populateLists():
	
	#Populate omega[]
	for i in range(1,N+1):
		omega.append(math.sqrt(4*K / M * math.sin(i*math.pi / (2.0*N+2))**2))

	#Populate particle[]values
	particle.append(Particle(0,0,0))
	for i in range(1,N+1):
		particle.append(Particle(i, math.sin(mode*i*math.pi/(N+1)), 0))
	particle.append(Particle(N+1,0,0))
	
def graph():
	f = open('results.txt', 'r')
	x = []
	y = []

	for i in range(N+1):
		y.append([])

	for line in f:
		arr = line.split(" ")
		t = float(arr.pop(0))
		x.append(t)
		for i in range(len(arr)):
			y[i].append(arr[i])
			#val = math.cos(mode*i*a - omega[mode-1]*t)
			#z[i].append(val)
	
	#yes we could loop here but this makes it easy to turn off plots by commenting them out
	if graph_type == True:

		plt.plot(x, y[0], label="1 Non-Linear")
		
		plt.plot(x, y[1], label="2 Non-Linear")
		
		plt.plot(x, y[2], label="3 Non-Linear")
		
		plt.plot(x, y[3], label="4 Non-Linear")
		
		plt.plot(x, y[4], label="5 Non-Linear")
		
		plt.plot(x, y[5], label="6 Non-Linear")
		
		plt.plot(x, y[6], label="7 Non-Linear")
		
		plt.plot(x, y[7], label="8 Non-Linear")
		

		plt.title('Displacement vs Time (Mode ' + str(mode) + ' initially excited)')
		plt.ylabel('Displacement')
		#plt.ylim(-2,2)
	else:
		plt.plot(x, y[0], label=1)
		plt.plot(x, y[1], label=2)
		plt.plot(x, y[2], label=3)
		plt.plot(x, y[3], label=4)
		plt.plot(x, y[4], label=5)
		plt.plot(x, y[5], label=6)
		plt.plot(x, y[6], label=7)
		plt.plot(x, y[7], label=8)
		'''	
		plt.plot(x, y[8], label=8)
		plt.plot(x, y[9], label=9)
		plt.plot(x, y[10], label=10)
		plt.plot(x, y[11], label=11)
		plt.plot(x, y[12], label=12)
		plt.plot(x, y[13], label=13)
		plt.plot(x, y[14], label=14)
		plt.plot(x, y[15], label=15)
		'''
		#plt.plot(x, y[N], label='Total')
		#plt.title('Energy vs Time (Mode ' + str(mode) + ' initially excited)')
		plt.ylabel('Energy')

	plt.xlabel('Time')
	#plt.legend(loc=1)	
	plt.xlim(0, finish_time)

	plt.show()


if GUI:
	pygame.init()
	pygame.display.set_caption('System 1')				#sets window title
	screen = pygame.display.set_mode((width, height)) 	#creates a window object, called screen
	font = pygame.font.Font(None, 36)					#font to draw text on screen


populateLists()

initE = getEnergy()
counter = 0
print("N: " + str(N))
print("K: " + str(K))
print("Alpha: " + str(alpha))
print("M: " + str(M))
print("dt: " + str(dt))
print("Mode: "+ str(mode))
print("Frequency: " + str(omega[mode]) + " rad/s")
print("Period: " + str(2*math.pi / omega[mode]))
finish_time = oscillations*2*math.pi/omega[mode]
print("Time to " + str(oscillations) + " oscillations: " + str(finish_time))
print("Initial Energy: " + str(initE))

running = True
if GUI:
	while running:
	   
	    for event in pygame.event.get():
	        if event.type == pygame.QUIT:
	            running = False
	    screen.fill(background_color)
	    updateParticles()
	    t = t + dt
	    e = getEnergy()
	    h = 0#getH()
	    error = getError(e, initE)
	    updateDisplay(t, e, error, h)
	    if t > finish_time:
	    	print("Final % error: " + str(error))
	    	print(str(oscillations) + " oscillations achieved")
	    	running = False
elif graph_type:
	f = open('results.txt', 'w')
	while running:
		updateParticles()
		t = t + dt
		msg = str(t) + " "
		for i in range(1,N+1):
			msg = msg + str(particle[i].x - particle[i].n) + " "
		f.write(msg + "\n")
		
		if t > finish_time:
			print("Final % error: " + str(getError(getEnergy(), initE)))
			print(str(oscillations) + " oscillations achieved")
			f.close()
			graph()
			running = False	
else:
	f = open('results.txt', 'w')
	while running:
		updateParticles()
		t = t + dt
		msg = str(t)
		for i in getModeEnergy():
			msg = msg + " " + str(i)
		f.write(msg + " " + str(getEnergy()) + "\n")

		if t > finish_time:
			print("Final % error: " + str(getError(getEnergy(), initE)))
			print(str(oscillations) + " oscillations achieved")
			f.close()
			graph()
			running = False	