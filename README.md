### About ###
This repository contains a simulation of a nonlinear 1d Bravais Lattice with Born-Von Karman Boundry Conditions

### Usage ###
To change simulation variables, the source code must be edited. An overview of the significance of each variable follows.

K: Spring Constant

alpha: Quadratic Non-linearity

beta: Cubic Non-linearity

M: Mass of all the particles in the system

N: The number of particles in the system

a: The spacing between each particle

dt: The change in time used for the simulation (smaller values are more accurate but take longer to run)

oscillations: How long the simulation should run, in terms of linear oscillations

graph_type: What graph should display at the end of the simulation. True = position vs time, False = energy vs time.

GUI: Whether or not the GUI should run (True or False). This is not used in the generation of data.

To run the simulation, use the following command:

`python System1.py [Initial mode number]`

To retrieve the graph of the previous simulation, copy the global variables used in System1.py into Graph.py, and run the following command:

`python Graph.py`
