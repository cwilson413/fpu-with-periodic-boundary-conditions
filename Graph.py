#Graphs data from results.txt
#9/10/14

import math, sys
import matplotlib.pyplot as plt

#Simulation Global Variables
K = 1.0 				#Spring Constant
alpha = 0.25 			#Non-linearity
beta = 0.0
M = 1.0 				#Mass of all particles
N = 8 					#Number of particles
a = 2.0 				#Spacing
t = 0 					#time
mode = 1				#starting mode number
dt = .01				#change in time for simulation
amp = 1.0 				#amplitude of initial position displacement
oscillations = 50		#Number of oscillations to simulate
particle = []			#list of particles
k = []					#list of k values
omega = [] 				#list of omega values
graph_type = False		#True = position vs time, False = energy vs time

def populateLists():
	#Populate k[]
	if N % 2 ==0:
		for i in range(N):
	 	   k.append(-math.pi/a+ i* 2*math.pi/N/a)			
	else:
		for i in range(N):
			k.append(-math.pi/a + 2*math.pi/N/a/2 + i*2*math.pi/N/a)

	#Populate omega[]
	for i in k:
		omega.append(math.sqrt(2 * (K/M) * (1.0 - math.cos(a*i))))


def graph():
	f = open('results.txt', 'r')
	x = []
	y = []
	z = []

	for i in range(N+1):
		y.append([])
		z.append([])

	for line in f:
		arr = line.split(" ")
		t = float(arr.pop(0))
		x.append(t)
		for i in range(len(arr)):
			y[i].append(arr[i])
			val = math.cos(k[mode]*i*a - omega[mode]*t)
			z[i].append(val)
	
	#yes we could loop here but this makes it easy to turn off plots by commenting them out
	if graph_type == True:

		plt.plot(x, y[0], label="0 Non-Linear")
		plt.plot(x, z[0], label="0 Linear")
		'''				
		plt.plot(x, y[1], label="1 Non-Linear")
		plt.plot(x, z[1], label="1 Linear")

		plt.plot(x, y[2], label="2 Non-Linear")
		plt.plot(x, z[2], label="2 Linear")

		plt.plot(x, y[3], label="3 Non-Linear")
		plt.plot(x, z[3], label="3 Linear")

		plt.plot(x, y[4], label="4 Non-Linear")
		plt.plot(x, z[4], label="4 Linear")

		plt.plot(x, y[5], label="5 Non-Linear")
		plt.plot(x, z[5], label="5 Linear")

		plt.plot(x, y[6], label="6 Non-Linear")
		plt.plot(x, z[6], label="6 Linear")

		plt.plot(x, y[7], label="7 Non-Linear")
		plt.plot(x, z[7], label="7 Linear")
		'''	

		plt.title('Displacement vs Time (Mode ' + str(mode) + ' initially excited)')
		plt.ylabel('Displacement')
		plt.ylim(-2,2)
	else:
		plt.plot(x, y[0], label=0)
		plt.plot(x, y[1], label=1)
		plt.plot(x, y[2], label=2)
		plt.plot(x, y[3], label=3)
		#plt.plot(x, y[4], label=4)
		#plt.plot(x, y[5], label=5)
		#plt.plot(x, y[6], label=6)
		#plt.plot(x, y[7], label=7)
		plt.plot(x, y[8], label='Total')
		plt.title('Energy vs Time (Mode ' + str(mode) + ' initially excited)')
		plt.ylabel('Energy')

	plt.xlabel('Time')
	plt.legend(loc=1)	
	plt.xlim(0, finish_time)

	plt.show()

populateLists()
finish_time = oscillations*2*math.pi/omega[mode]
graph()